import 'package:bio_app/bio_text.dart';
import 'package:flutter/material.dart';
class BioCard extends StatelessWidget {
  final double elevation;
  final double radius;
  final Color borderColor;
  final double borderWidth;
  final IconData leadingIcon;
  final String title;
  final String subTitle;
  final IconData trailingIcon;
  final VoidCallback? onPressed;
  final double bottomMargin;

  BioCard({
    this.elevation=6,
    this.radius=20,
    this.borderColor=Colors.transparent,
    this.borderWidth=0,
    required this.leadingIcon,
    required this.title,
    required this.subTitle,
    required this.trailingIcon,
    this.onPressed,
    this.bottomMargin=10,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: elevation,
      margin: EdgeInsetsDirectional.only(
        bottom: bottomMargin,
      ),
      shape: RoundedRectangleBorder(
        borderRadius:BorderRadius.circular(radius),
        side:BorderSide(
          color:borderColor,
          width:borderWidth,
        ),
      ),
      child:ListTile(
        leading: Icon(leadingIcon),
        title: BioText(
          text:title,
        ),
        subtitle: BioText(
          text: subTitle,
          color:Colors.black,
          fontSize: 12,
        ),
        trailing: IconButton(
          icon: Icon(trailingIcon),
          onPressed: onPressed,
        ),
      ),

    );
  }
}
