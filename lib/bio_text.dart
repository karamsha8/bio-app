import 'package:flutter/material.dart';

class BioText extends StatelessWidget {
  final String text;
  final double fontSize;
  final TextDecoration? decoration;
  final TextDecorationStyle? decorationStyle;
  final Color color;
  final TextAlign? textAlign;
  BioText({
    required this.text,
    this.fontSize=17,
    this.decoration=TextDecoration.underline,
    this.decorationStyle=TextDecorationStyle.wavy,
    this.color=Colors.blue,
    this.textAlign,
  });
  @override
  Widget build(BuildContext context) {
    return Text(
      text ,
      textAlign:textAlign,
      style: TextStyle(
        fontSize: fontSize,
        decoration: decoration,
        decorationStyle: decorationStyle,
        color: color,
      ),
    );
  }
}
