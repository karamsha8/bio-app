import 'package:bio_app/bio_card.dart';
import 'package:bio_app/bio_text.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'BioApp',
          style: TextStyle(
            color: Colors.black38,
          ),
        ),
        backgroundColor: Colors.white,


      ),
      body: Padding(
        padding:EdgeInsetsDirectional.only(
          start: 30,
          end: 30,
        ),
        child: Column(
          children: [
            CircleAvatar(
              radius: 30,
              backgroundImage:AssetImage('images/karam.jpg')
            ),
            SizedBox(
              height: 10,
            ),
            BioText(text: 'Karam Abu Amsha'),
            BioText(
              text: 'Pallancer',
              color: Colors.red,
            ),
            Divider(
              color: Colors.black,
              indent: 15,
              endIndent: 15,
              thickness: 1,
              height: 20,
            ),
            BioCard(
              leadingIcon: Icons.email,
              title: 'Email',
              subTitle: 'Karamsh8@gmail.com',
              trailingIcon: Icons.send,
              borderColor: Colors.red,
              borderWidth: 1,
            ),
            BioCard(
              leadingIcon: Icons.call,
              title: 'Mobiel',
              subTitle: '0597831892',
              trailingIcon: Icons.send,
              borderColor: Colors.red,
              borderWidth: 1,
            ),
          ],
        ),
      ),
    );
  }
}
