import 'dart:ui';

import 'package:bio_app/bio_text.dart';
import 'package:bio_app/home_screen.dart';
import 'package:flutter/material.dart';
class LaunchScreen extends StatefulWidget {

  @override
  _LaunchScreenState createState() => _LaunchScreenState();
}

class _LaunchScreenState extends State<LaunchScreen> with SingleTickerProviderStateMixin {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 3),(){
      Navigator.pushReplacement(
        context, MaterialPageRoute(
        builder: (context) => HomeScreen(),
      ),);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black38,
      body: Stack(
        children: [
          ImageFiltered(
            imageFilter: ImageFilter.blur(
              sigmaX:3,
              sigmaY: 3,
            ),
            child: Image.asset(
              'images/ucas.png',
              fit: BoxFit.cover,
              height: double.infinity,
            ),
          ),
          Center(
            child: BioText(
              text: 'PalLancer',
              color: Colors.red,
              fontSize: 30,
            ),
          ),
        ],
      ),
    );
  }
}
