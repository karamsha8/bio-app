import 'package:bio_app/home_screen.dart';
import 'package:bio_app/launch.dart';
import 'package:flutter/material.dart';

void main()=>runApp(Main());
class Main extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/launch_screen',
      routes: {
        '/launch_screen':(context)=> LaunchScreen(),
        '/home_screen':(context)=> HomeScreen(),
      },
    );
  }
}
