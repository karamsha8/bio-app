import 'package:flutter/material.dart';
class AppSettings{
  static TextStyle generateTextStyle({
    Color textColor = Colors.black,
    double fontSize=20,
  }){
    return TextStyle(
      color: textColor,
      fontSize: fontSize,
    );
  }
}